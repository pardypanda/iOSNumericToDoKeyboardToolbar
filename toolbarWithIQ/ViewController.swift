//
//  ViewController.swift
//  toolbarWithIQ
//
//  Created by Pardy Panda 09 on 27/04/18.
//  Copyright © 2018 Pardy Panda 09. All rights reserved.
//

import UIKit
import RFKeyboardToolbar
import IQKeyboardManager

var tfLast : UITextField?

class ViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var txtview: UITextView!
    @IBOutlet weak var temi: UITextField!
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var txtToolbar: UITextField!
   var cursorPosition = Int()
    var keyboardToolbar = RFKeyboardToolbar()
    var buttonTitle = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0","$","⬜","✅"]
    var buttons = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        IQKeyboardManager.shared().isEnableAutoToolbar = true
        
        // Make buttons for toolbar
        for i in 0...12 {
            
            let button = RFToolbarButton(title: buttonTitle[i], andEventHandler: {() -> Void in
                
                // First Responder for textField.
                if let txtfield = self.containerView.currentFirstResponder() as? UITextField{
                    print("its type of  textfield\(String(describing: txtfield.text))")
                    self.textfieldWithNumericAndEmoji(textfield: txtfield, i: i)
                }
                
                // First Responder for textView
                if let txtview = self.containerView.currentFirstResponder() as? UITextView {
                    print("its a textview\(txtview.text)")
                    self.textViewWithNumericAndEmoji(textview: txtview, i: i)
                }
                
                
            }, for: .touchUpInside)
            buttons.append(button!)
        }
        
        keyboardToolbar = RFKeyboardToolbar(buttons: buttons)
        txtToolbar.inputAccessoryView = keyboardToolbar
        temi.inputAccessoryView = keyboardToolbar
        txtview.inputAccessoryView = keyboardToolbar
        temi.delegate = self
        txtview.delegate = self
        txtToolbar.delegate = self
    }
    
    // if first responder is textfield.
    func textfieldWithNumericAndEmoji(textfield: UITextField, i: Int) {
     
        if i == 11 {
            if (textfield.text?.contains("⬜"))!{
                
            }else if (textfield.text?.contains("✅"))! {
                let value = textfield.text?.replacingOccurrences(of: "✅", with: "⬜", options: .literal, range: nil)
                textfield.text = value
            }else{
                let value = textfield.text
                textfield.text = "\(self.buttonTitle[i])\(value!)"
            }
        }else if i == 12 {
            if (textfield.text?.contains("✅"))!{
                
            }else if (textfield.text?.contains("⬜"))! {
                let value = textfield.text?.replacingOccurrences(of: "⬜", with: "✅", options: .literal, range: nil)
                textfield.text = value
            }else{
                let value = textfield.text
                textfield.text = "\(self.buttonTitle[i])\(value!)"
            }
        }else{
            if let selectedRange = textfield.selectedTextRange {
                 cursorPosition = textfield.offset(from: self.txtToolbar.beginningOfDocument, to: selectedRange.start)
                print("\(cursorPosition)")
                print("\(textfield.text?.count)")
            }
            if cursorPosition < (textfield.text?.count)!  {
                print("Less then")
                let str = textfield.text as! String
                let index = str.index((str.startIndex), offsetBy: cursorPosition)
                print("\(str[..<index])")
                let frontText = str[..<index]
                let backtext = str.replacingOccurrences(of: frontText, with: "", options: [.anchored], range: nil)
                print("\(backtext)")
                textfield.text = "\(frontText)\(self.buttonTitle[i])\(backtext)"
                if let selectedRange = textfield.selectedTextRange {
                    // and only if the new position is valid
                    if let newPosition = textfield.position(from: selectedRange.start, offset: -backtext.count) {
                        // set the new position
                        textfield.selectedTextRange = textfield.textRange(from: newPosition, to: newPosition)
                    }
                }
            }else{
                let value = textfield.text
                textfield.text = "\(value!)\(self.buttonTitle[i])"
            }
         
   
        }
    }
    
    // if first responder is textview.
    func textViewWithNumericAndEmoji(textview: UITextView, i: Int) {
        
        if i == 11 {
            if (textview.text?.contains("⬜"))!{
                
            }else if (textview.text?.contains("✅"))! {
                let value = textview.text?.replacingOccurrences(of: "✅", with: "⬜", options: .literal, range: nil)
                textview.text = value
            }else{
                let value = textview.text
                textview.text = "\(self.buttonTitle[i])\(value!)"
            }
        }else if i == 12 {
            if (textview.text?.contains("✅"))!{
                
            }else if (textview.text?.contains("⬜"))! {
                let value = textview.text?.replacingOccurrences(of: "⬜", with: "✅", options: .literal, range: nil)
                textview.text = value
            }else{
                let value = textview.text
                textview.text = "\(self.buttonTitle[i])\(value!)"
            }
        }else{
            if let selectedRange = textview.selectedTextRange {
                cursorPosition = textview.offset(from: self.txtToolbar.beginningOfDocument, to: selectedRange.start)
            }
            if cursorPosition < (textview.text?.count)!  {
                print("Less then")
                let str = textview.text as! String
                let index = str.index((str.startIndex), offsetBy: cursorPosition)
                print("\(str[..<index])")
                let frontText = str[..<index]
                let backtext = str.replacingOccurrences(of: frontText, with: "", options: [.anchored], range: nil)
                print("\(backtext)")
                textview.text = "\(frontText)\(self.buttonTitle[i])\(backtext)"
                if let selectedRange = textview.selectedTextRange {
                    // and only if the new position is valid
                    if let newPosition = textview.position(from: selectedRange.start, offset: -backtext.count) {
                        // set the new position
                        textview.selectedTextRange = textview.textRange(from: newPosition, to: newPosition)
                    }
                }
            }else{
                let value = textview.text
                textview.text = "\(value!)\(self.buttonTitle[i])"
            }

        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
extension UIView {
    // First responder of the view
    func currentFirstResponder() -> UIResponder? {
        if self.isFirstResponder {
            return self
        }
        
        for view in self.subviews {
            if let responder = view.currentFirstResponder() {
                return responder
            }
        }
        
        return nil
    }
}


