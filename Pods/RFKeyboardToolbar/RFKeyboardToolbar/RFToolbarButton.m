//
//  RFToolbarButton.m
//
//  Created by Rudd Fawcett on 12/3/13.
//  Copyright (c) 2013 Rudd Fawcett. All rights reserved.
//

#import "RFToolbarButton.h"

@interface RFToolbarButton ()

/**
 *  The button's title.
 */
@property (nonatomic, strong) NSString *title;

/**
 *  The button's event block.
 */
@property (nonatomic, copy) eventHandlerBlock buttonPressBlock;

@end

@implementation RFToolbarButton

+ (instancetype)buttonWithTitle:(NSString *)title {
    return [[self alloc] initWithTitle:title];
}

+ (instancetype)buttonWithTitle:(NSString *)title andEventHandler:(eventHandlerBlock)eventHandler forControlEvents:(UIControlEvents)controlEvent {
    RFToolbarButton *newButton = [RFToolbarButton buttonWithTitle:title];
    [newButton addEventHandler:eventHandler forControlEvents:controlEvent];
    
    return newButton;
}

- (id)initWithTitle:(NSString *)title {
    _title = title;
    return [self init];
}

- (id)init {
    CGSize sizeOfText = [self.title sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:20.f]}];
    CGFloat width = 0;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        
        switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
                
            case 1136:
                printf("iPhone 5 or 5S or 5C");
                width = sizeOfText.width + 11;
                break;
            case 1334:
                printf("iPhone 6/6S/7/8");
                width = sizeOfText.width + 15;
                break;
            case 1920:
                printf("iPhone 6+/6S+/7+/8++");
                width = sizeOfText.width + 15;
                break;
            case 2208:
                printf("iPhone 6+/6S+/7+/8+");
                width = sizeOfText.width + 18;
                break;
            case 2436:
                printf("iPhone X");
                width = sizeOfText.width + 15;
                break;
            default:
                printf("unknown");
        }
    }
    if (self = [super initWithFrame:CGRectMake(0, 0, width, sizeOfText.height + 17)]) {
        self.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
        
        self.layer.cornerRadius = 5.0f;
        self.layer.borderWidth = 1.0f;
        self.layer.borderColor = [UIColor colorWithWhite:0.8 alpha:1.0].CGColor;
        
        [self setTitle:self.title forState:UIControlStateNormal];
        [self setTitleColor:[UIColor colorWithWhite:0.0 alpha:1.0] forState:UIControlStateNormal];
        
        self.titleLabel.font = [UIFont systemFontOfSize:20.f];
        self.titleLabel.textColor = [UIColor colorWithWhite:0.500 alpha:1.0]; //206 209 214
    }
    return self;
}

- (void)addEventHandler:(eventHandlerBlock)eventHandler forControlEvents:(UIControlEvents)controlEvent {
    self.buttonPressBlock = eventHandler;
    [self addTarget:self action:@selector(buttonPressed) forControlEvents:controlEvent];
}

- (void)buttonPressed {
    self.buttonPressBlock();
}

@end
